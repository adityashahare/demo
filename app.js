const express = require('express')
const app = express()
const port = 8000

app.get('/', (req, res) => {
    res.send('Sample CICD Pipline is this!')
})

app.listen(port, () =>{
    console.log('Application is listening at http://localhost:${port}')
})